a = input('Ввведите операцию, используя метод "Польской нотации" ')
b = a.split(' ')

# assert '+' in b[0]
# print('Неверный оператор')

if b[0] == '+':
  # assert '+' in b[0]
  # AssertionError
  # print('Неверный оператор')
  try:
    result = int(b[1]) + int(b[2])
  except ValueError:
    result = 'Некорректный ввод'   
  print(result)
elif b[0] == '-':
  try:
    result = int(b[1]) - int(b[2])
  except ValueError:
    result = 'Некорректный ввод'   
  print(result)

elif b[0] == '*':
  try:
    result = int(b[1]) * int(b[2])
  except ValueError:
    result = 'Некорректный ввод'   
  print(result)

elif b[0] == '/':
  try:
    result = int(b[1]) / int(b[2])
  except ZeroDivisionError:
    result = 'На ноль делить нельзя!'
  except ValueError:
    result = 'Некорректный ввод'    
  print(result)

else:
  print('Опрация невозможна')
