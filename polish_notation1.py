a = input('Ввведите операцию, используя метод "Польской нотации" ')
b = a.split(' ')

#AssertionError
b[0] == '+'
assert b[0] in ['+', '-', '*', '/'], 'Оператор неправильно набран'
print(b)

try:
  if b[0] == '+':
    result = int(b[1]) + int(b[2])
  elif b[0] == '-':
    result = int(b[1]) - int(b[2])
  elif b[0] == '*':
    result = int(b[1]) * int(b[2])
  elif b[0] == '/':
    result = int(b[1]) / int(b[2])
  print(result)
except ZeroDivisionError:
    print('На ноль делить нельзя!')
except ValueError:
    print('Неправильный ввод')
except IndexError:
    print('Неправильное количество аргументов')