import csv
import datetime

class ApartmentCounter:

  def __init__(self, app):
    self.app = app
    self.create_time = datetime.datetime.now()

  def __enter__(self):
    self.file = open(self.app, 'w')
    return self.file

  def __exit__(self, exc_type, exc_val, exc_tb):
    print(exc_type)
    print(exc_val)
    print(exc_tb)
    print(dir(exc_type))
    self.file.close()
    self.end_time = datetime.datetime.now()
    print(self.end_time - self.create_time)
if __name__ == '__main__':

  with ApartmentCounter('csv') as file:
    for i in range(2000000):

      file.write(f'(towrite)\n')
  print('x')