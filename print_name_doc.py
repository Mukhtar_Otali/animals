documents = [
    {"type": "passport", "number": "2207 876234", "name": "Василий Гупкин"},
    {"type": "invoice", "number": "11-2", "name": "Геннадий Покемонов"},
    {"type": "insurance", "number": "10006", "name": "Аристарх Павлов"}
]

# # Перечень полок, на которых находятся документы хранится в следующем виде:

directories = {
    '1': ['2207 876234', '11-2'],
    '2': ['10006'],
    '3': []
}


# doc_num = 22

# directories = {
#   '1': [22, 55, 66],
#   '2': [33, 10006],
# }
# for directory in directories.keys():
#   if doc_num in directories[directory]:
#     print(f'Документ на {directory} полке')

def find_doc(number):
    result = 'Ничего не найдено'
    for item in documents:
        if number == item['number']:
            result = 'Имя: ' + item['name']
    print(result)


def print_docs(documents):
    for item in documents:
        print(item['type'], item['number'], item['name'])


def find_shelf(find_number):
    result = 'Ничего не найдено'
    for key, value in directories.items():
        if find_number in value:
            result = 'Номер полки: ' + key
    print(result)


def add_doc(documents, directories):
    add_type = input('Введите тип документа:')
    add_number = input('Введите номер документа:')
    add_name = input('Введите имя владельца документа:')
    add_shelf = input('На какую полку положить?:')

    add_doc = {'type': add_type, 'number': add_number, 'name': add_name}
    documents.append(add_doc)
    if add_shelf in directories.keys():
        directories[add_shelf].append(add_number)
        print('Документ добавлен')
    else:
        print('Полки с таким номером нет!')


def print_name_docs(documents):
    for item in documents:
        try:
            print(item['name'])
        except KeyError as e:
            print(e)


function = input('Какую команду нужно выполнить?')
if function == 'p':
    number = input('Введите номер документа:')
    find_doc(number)
elif function == 'l':
    print_docs(documents)
elif function == 's':
    find_number = input('Введите номер документа:')
    find_shelf(find_number)
elif function == 'a':
    add_doc(documents, directories)
elif function == 'n':
    print_name_docs(documents)
else:
    print('Такой функции не существует')