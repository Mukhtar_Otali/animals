class Pets():
    def __init__(self, name=None, weight=None):
        self.name = name
        self.weight = weight
        self.wool = 100

    def __radd__(self, other):
        if isinstance(other, float) or isinstance(other, int):
            return self.weight + other

    def __lt__(self, other):
        if isinstance(other, Pets):
            return self.weight < other.weight


class DomesticBirds(Pets):
    def cut(self, quantity: None):
        print('Птиц стричь нельзя')


bird = DomesticBirds('Серая')
print(f'Птичка {bird.name} создана')

cow_Manka = Pets('Манька', 300)
sheep_Lamb = Pets('Ягнёнок', 50)
sheep_Kurly = Pets('Кудрявый', 60)
goat_Horns = Pets('Рога', 40)
goat_Hooves = Pets('Копыта', 45)
goose1 = Pets('Серый', 8)
goose2 = Pets('Белый', 6)
duck = Pets('Утка', 4)

pets = [cow_Manka, sheep_Lamb, sheep_Kurly, goat_Horns, goat_Hooves, goose1, goose2, duck]

print('Общий вес животных', sum(pets), 'кг')

print('Самое тяжелое животное', max(pets).name)


class Hen(DomesticBirds):
    def __str__(self):
        return f'{self.name} кудахчет "Кококо"'


ryaba = Hen('Ряба')
print(ryaba)