a = input('Ввведите операцию, используя метод "Польской нотации" ')
b = a.split(' ')

if b[0] == '+':
  result = int(b[1]) +int(b[2])
  print(result)

elif b[0] == '-':
  result = int(b[1]) - int(b[2])
  print(result)

elif b[0] == '*':
  result = int(b[1]) * int(b[2])
  print(result)

elif b[0] == '/':
  result = int(b[1]) / int(b[2])
  print(result)

else:
  print('Опрация невозможна')
