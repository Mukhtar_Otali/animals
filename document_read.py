import random
import datetime

class DocumentReader:

  def __init__(self, page):
    self.page = page
    self.create_time = datetime.datetime.now()

  def __enter__(self):
    self.file = open(self.page, 'w')
    return self.file

  def __exit__(self, exc_type, exc_val, exc_tb):
    print(exc_type)
    print(exc_val)
    print(exc_tb)
    print(dir(exc_type))
    self.file.close()
    self.end_time = datetime.datetime.now()
    print(self.end_time - self.create_time)
if __name__ == '__main__':

  # with DocumentReader('random.txt') as file:
  #   for i in range(200):
  #     towrite = str(random.randint(1, 200))

  #     file.write(f'(towrite)\n')
  # print('x')

  with DocumentReader('random.txt') as file:
    for i in range(2000000):
      towrite = str(random.randint(1, 200))

      file.write(f'(towrite)\n')
  print('x')
